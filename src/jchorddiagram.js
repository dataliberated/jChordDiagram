import * as d3 from "d3";

$.fn.ChordDiagram = function(config) {
	var chordDiagramObject;

	// Initialiser
	chordDiagramObject = new ChordDiagram(this, config);

	return this;
};

// Declare prototype object for chord diagrams.
function ChordDiagram(selecti, config) {

	var headerFontSize = config.headerFontSize !== undefined ? config.headerFontSize : 50;
	var entryFontSize = config.entryFontSize !== undefined ? config.entryFontSize : 35;
	var fontColor = config.fontColor !== undefined ? config.fontColor : "#000000";
	var headerHeight = null;
	var entryHeight = null;

	//
	// Private variables.
	//

	// Holds root DOM element in which to draw.
	var element = selecti[0];

	var legend = null;

	var data = config.data;

	//
	// Private methods.
	//

	var color = function(i) {
		if ( !config.colours || !config.colours.length ) return "#000000";
		return (config.colours[i % config.colours.length]);
	};

	var mouseOverHandler = function(d, i) {
		var svg = d3.select(element).select("svg");
		d3.selectAll(".chord")
			.attr("fill", function(de) {
				return color(de.target.index);
			})
			.attr("stroke", function(de) {
				return d3.rgb(color(de.target.index)).darker();
			})
			.attr("opacity", 0.8);

		var chords = svg.selectAll(".chord");
		chords
			.filter(function(de) {
				return de.source.index !== i && de.target.index !== i;
			})
			.attr("opacity", 0.1);

		chords
			.filter(function(de) {
				return de.source.index === i || de.target.index === i;
			})
			.attr("fill", color(d.index))
			.attr("stroke", d3.rgb(color(d.index)).darker());

		if (config.legend) {
			d3.select(element).selectAll("svg.legend *").remove();

			var f = d3.format(config.valueFormat != null ? config.valueFormat : ".2f");
			var dataArray = [];
			for (var j = 0; j < data.labels.length; j++) {
				if (i !== j) {
					dataArray.push({
						key: data.labels[j],
						value: "" + config.valuePrefix + f(data.matrix[i][j]),
						color: color(j)
					});
				}
			}

			/* Header marker. */
			d3.select(element)
				.select("svg.legend")
				.append("rect")
				.attr("fill", color(i))
				.attr("y", 0)
				.attr("x", 0)
				.attr("width", headerHeight)
				.attr("height", headerHeight)
				.attr("rx", headerHeight / 10)
				.attr("ry", headerHeight / 10);

			/* Header text. */
			d3.select(element)
				.select("svg.legend")
				.append("text")
				.text(data.labels[i])
				.attr("font-size", headerFontSize)
				.attr("dominant-baseline", "hanging")
				.attr("y", 0)
				.attr("x", 1.1 * headerHeight)
				.attr("fill", fontColor);

			/* Entry markers. */
			d3.select(element)
				.select("svg.legend")
				.selectAll("rect.legend-marker")
				.data(dataArray)
				.enter()
				.append("rect")
				.attr("class", "legend-marker")
				.attr("fill", function(d) {
					return d.color;
				})
				.attr("y", function(d, i) {
					return 1.3 * headerHeight + i * 1.3 * entryHeight;
				})
				.attr("x", 0)
				.attr("width", entryHeight)
				.attr("height", entryHeight)
				.attr("rx", entryHeight / 10)
				.attr("ry", entryHeight / 10);

			/* Entry text. */
			d3.select(element)
				.select("svg.legend")
				.selectAll("text.legend-entry")
				.data(dataArray)
				.enter()
				.append("text")
				.text(function(d) {
					return d.key + ": " + d.value;
				})
				.attr("class", "legend-entry")
				.attr("font-size", entryFontSize)
				.attr("font-weight", 100)
				.attr("y", function(d, i) {
					return 1.3 * headerHeight + i * 1.3 * entryHeight;
				})
				.attr("x", 1.1 * entryHeight)
				.attr("dominant-baseline", "hanging")
				.attr("fill", fontColor);

			var height = 1.3 * headerHeight + (data.labels.length - 1) * (1.3 * entryHeight);
			var width = legend.node()
				.getBBox()
				.width;

			legend.attr("viewBox", "0 0 " + width + " " + height)
				.attr("preserveAspectRatio", "xMidYMid meet");
			if (config.legendCallback != null) {
				config.legendCallback(legend, d);
			}

		}
	};

	var mouseOutHandler = function() {};


	var clickHandlerWrapper = function(d, i) {
		if (config.onClick != null) {
			config.onClick(data.labels[i]);
		}
	};

	var draw = function() {

		var svgroot = d3.select(element).append("svg")
			.attr("class", "diagram")
			.attr("preserveAspectRatio", "xMidYMid meet");
		var svgWidth = 101;
		var svgHeight = 101;
		var outerRadius = 50;
		var svg = svgroot.append("g")
			.append("g")
			.attr("class", "chordgraph")
			.attr("transform", "translate(" + svgWidth / 2 + "," + svgHeight / 2 + ")");


		svgroot
			.attr("viewBox", "0 0 " + svgWidth + " " + svgHeight);

		//set color scale. More color options: https://github.com/d3/d3-scale-chromatic

		//d3 chord generator
		var chord = d3.chord()
			.padAngle(0.05)
			.sortSubgroups(d3.descending);

		//apply the matrix
		var chords = chord(data.matrix);

		//each ribbon generator
		var ribbon = d3.ribbon()
			.radius(outerRadius / 1.1);

		//outer rim arc
		var arc = d3.arc()
			.innerRadius(outerRadius / 1.1)
			.outerRadius(outerRadius);

		//add each of the groupings for outer rim arcs
		var group = svg.append("g")
			.selectAll("g")
			.data(chords.groups)
			.enter()
			.append("g");

		//add each outer rim arc path
		group.append("path")
			.attr("fill", function(d) {
				return color(d.index);
			})
			.attr("stroke", function(d) {
				return color(d.index);
			})
			.attr("stroke-width", 0)
			.attr("class", "chord-tab")
			.attr("d", arc)
			.style("cursor", "pointer")
			.on("mouseover", mouseOverHandler)
			.on("mouseout", mouseOutHandler)
			.on("click", clickHandlerWrapper);

		//add each ribbon
		svg.append("g")
			.selectAll("path")
			.data(chords)
			.enter()
			.append("path")
			.attr("d", ribbon)
			.attr("class", "chord")
			.attr("fill", function(d) {
				return color(d.target.index);
			})
			.attr("stroke", function(d) {
				return d3.rgb(color(d.target.index)).darker();
			})
			.attr("stroke-width", 0)
			.attr("opacity", 0.8);

		if (config.legend) {

			// Dynamically create legend markup
			legend = d3.select(element).append("svg")
				.attr("class", "legend");

			/* Pre-calculate text dimensions. */
			var calculationArea = d3.select(element)
				.selectAll("svg.text-calculation-area")
				.data([null])
				.enter()
				.append("svg")
				.attr("class", "text-calculation-area")
				.style("position", "absolute")
				.style("left", "0")
				.style("top", "0")
				.style("visibility", "hidden");

			headerHeight = calculationArea
				.append("text")
				.attr("font-size", headerFontSize)
				.attr("dominant-baseline", "hanging")
				.text("placeholder text")
				.node()
				.getBBox()
				.height;
			entryHeight = calculationArea
				.append("text")
				.attr("font-size", entryFontSize)
				.attr("dominant-baseline", "hanging")
				.text("placeholder text")
				.node()
				.getBBox()
				.height;

			var evnt = document.createEvent("SVGEvents");
			evnt.initEvent("mouseover", true, true);
			d3.select(element)
				.select(".chord-tab")
				.node()
				.dispatchEvent(evnt);
		}
	};


	//
	// Init
	//

	if (data == null || config.data == null) {
		console.error("You must supply a dataset in order to instantiate a chord diagram.");
	}


	draw();

}
