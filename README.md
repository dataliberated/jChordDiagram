# jChordDiagram

## Description
A small library for drawing chord diagrams from data matrices. 

## Dependencies
* **jQuery 3.x.x**
* **d3.js 5.x.x**

