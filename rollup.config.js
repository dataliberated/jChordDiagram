/* jChordDiagram
 * A small jQuery plugin for drawing chord charts. */

import {terser} from "rollup-plugin-terser";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";

var exportBuild = null;

// If light build, only build minified standalone.
if(process.env.LIGHT) {
	// Standalone build bundled with dependencies (excluding jQuery).
	exportBuild = {
		input: "src/jchorddiagram.js",
		output: {
			file: "dist/jchorddiagram.standalone.min.js",
			format: "iife",
			globals: {
				"window": "window",
				"document": "document",
				"jQuery": "$"
			},
			sourceMap: "inline"
		},
		external: ["jquery"],
		plugins: [
			resolve({
				jsnext: true,
				main: true,
				browser: true
			}),
			commonjs(),
			terser()
		]
	}
}
else {
	exportBuild = [
		// Regular build.
		{
			input: "src/jchorddiagram.js",
			output: {
				file: "dist/jchorddiagram.js",
				format: "iife",
				globals: {
					"window": "window",
					"document": "document",
					"jQuery": "$",
					"d3": "d3",
					"common-diagram-tooltip": "common-diagram-tooltip"
				},
				sourceMap: "inline"
			},
			external: ["jquery", "d3"],
		},

		// Standalone build bundled with dependencies (excluding jQuery).
		{
			input: "src/jchorddiagram.js",
			output: {
				file: "dist/jchorddiagram.standalone.js",
				format: "iife",
				globals: {
					"window": "window",
					"document": "document",
					"jQuery": "$"
				},
				sourceMap: "inline"
			},
			external: ["jquery"],
			plugins: [
				resolve({
					jsnext: true,
					main: true,
					browser: true
				}),
				commonjs(),
			]
		},

		// Minified regular build.
		{
			input: "dist/jchorddiagram.js",
			output: {
				file: "dist/jchorddiagram.min.js",
				format: "iife",
			},
			plugins: [
				terser()
			]
		},

		// Minified standalone build.
		{
			input: "dist/jchorddiagram.standalone.js",
			output: {
				file: "dist/jchorddiagram.standalone.min.js",
				format: "iife",
			},
			plugins: [
				terser()
			]
		}
	];
}

export default exportBuild;
